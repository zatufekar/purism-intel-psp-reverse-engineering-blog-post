!function(a,b){function c(){function a(){"undefined"!=typeof _wpmejsSettings&&(c=b.extend(!0,{},_wpmejsSettings)),c.classPrefix="mejs-",c.success=c.success||function(a){var b,c;a.rendererName&&-1!==a.rendererName.indexOf("flash")&&(b=a.attributes.autoplay&&"false"!==a.attributes.autoplay,c=a.attributes.loop&&"false"!==a.attributes.loop,b&&a.addEventListener("canplay",function(){a.play()},!1),c&&a.addEventListener("ended",function(){a.play()},!1))},c.customError=function(a,b){if(-1!==a.rendererName.indexOf("flash")||-1!==a.rendererName.indexOf("flv"))return'<a href="'+b.src+'">'+mejsL10n.strings["mejs.download-video"]+"</a>"},b(".wp-audio-shortcode, .wp-video-shortcode").not(".mejs-container").filter(function(){return!b(this).parent().hasClass("mejs-mediaelement")}).mediaelementplayer(c)}var c={};return{initialize:a}}a.wp=a.wp||{},a.wp.mediaelement=new c,b(a.wp.mediaelement.initialize)}(window,jQuery);
/*
     FILE ARCHIVED ON 04:44:18 Apr 07, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:05:01 May 11, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 249.008 (3)
  esindex: 0.02
  captures_list: 279.328
  CDXLines.iter: 16.739 (3)
  PetaboxLoader3.datanode: 256.077 (4)
  exclusion.robots: 0.656
  exclusion.robots.policy: 0.568
  RedisCDXSource: 6.303
  load_resource: 15.307
*/