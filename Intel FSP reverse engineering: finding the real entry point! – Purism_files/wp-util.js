window.wp=window.wp||{},function(a){var b="undefined"==typeof _wpUtilSettings?{}:_wpUtilSettings;wp.template=_.memoize(function(b){var c,d={evaluate:/<#([\s\S]+?)#>/g,interpolate:/\{\{\{([\s\S]+?)\}\}\}/g,escape:/\{\{([^\}]+?)\}\}(?!\})/g,variable:"data"};return function(e){return(c=c||_.template(a("#tmpl-"+b).html(),d))(e)}}),wp.ajax={settings:b.ajax||{},post:function(a,b){return wp.ajax.send({data:_.isObject(a)?a:_.extend(b||{},{action:a})})},send:function(b,c){var d,e;return _.isObject(b)?c=b:(c=c||{},c.data=_.extend(c.data||{},{action:b})),c=_.defaults(c||{},{type:"POST",url:wp.ajax.settings.url,context:this}),e=a.Deferred(function(b){c.success&&b.done(c.success),c.error&&b.fail(c.error),delete c.success,delete c.error,b.jqXHR=a.ajax(c).done(function(a){"1"!==a&&1!==a||(a={success:!0}),_.isObject(a)&&!_.isUndefined(a.success)?b[a.success?"resolveWith":"rejectWith"](this,[a.data]):b.rejectWith(this,[a])}).fail(function(){b.rejectWith(this,arguments)})}),d=e.promise(),d.abort=function(){return e.jqXHR.abort(),this},d}}}(jQuery);
/*
     FILE ARCHIVED ON 04:44:11 Apr 07, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:05:01 May 11, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 586.94 (3)
  esindex: 0.011
  captures_list: 634.33
  CDXLines.iter: 17.823 (3)
  PetaboxLoader3.datanode: 611.172 (4)
  exclusion.robots: 0.401
  exclusion.robots.policy: 0.344
  RedisCDXSource: 22.491
  PetaboxLoader3.resolve: 65.1
  load_resource: 93.471
*/