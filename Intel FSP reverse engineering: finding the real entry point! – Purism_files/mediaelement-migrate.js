!function(a,b){void 0===mejs.plugins&&(mejs.plugins={},mejs.plugins.silverlight=[],mejs.plugins.silverlight.push({types:[]})),mejs.HtmlMediaElementShim=mejs.HtmlMediaElementShim||{getTypeFromFile:mejs.Utils.getTypeFromFile},void 0===mejs.MediaFeatures&&(mejs.MediaFeatures=mejs.Features),void 0===mejs.Utility&&(mejs.Utility=mejs.Utils);var c=MediaElementPlayer.prototype.init;MediaElementPlayer.prototype.init=function(){this.options.classPrefix="mejs-",this.$media=this.$node=b(this.node),c.call(this)};var d=MediaElementPlayer.prototype._meReady;MediaElementPlayer.prototype._meReady=function(){this.container=b(this.container),this.controls=b(this.controls),this.layers=b(this.layers),d.apply(this,arguments)},MediaElementPlayer.prototype.getElement=function(a){return void 0!==b&&a instanceof b?a[0]:a},MediaElementPlayer.prototype.buildfeatures=function(a,c,d,e){for(var f=["playpause","current","progress","duration","tracks","volume","fullscreen"],g=0,h=this.options.features.length;g<h;g++){var i=this.options.features[g];if(this["build"+i])try{f.indexOf(i)===-1?this["build"+i](a,b(c),b(d),e):this["build"+i](a,c,d,e)}catch(j){console.error("error building "+i,j)}}}}(window,jQuery);
/*
     FILE ARCHIVED ON 04:44:17 Apr 07, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:05:01 May 11, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 266.676 (3)
  esindex: 0.012
  captures_list: 290.853
  CDXLines.iter: 16.515 (3)
  PetaboxLoader3.datanode: 289.813 (4)
  exclusion.robots: 0.366
  exclusion.robots.policy: 0.313
  RedisCDXSource: 1.464
  PetaboxLoader3.resolve: 53.074
  load_resource: 89.743
*/