jQuery(function(o){o(".woocommerce-ordering").on("change","select.orderby",function(){o(this).closest("form").submit()}),o("input.qty:not(.product-quantity input.qty)").each(function(){var e=parseFloat(o(this).attr("min"));e>=0&&parseFloat(o(this).val())<e&&o(this).val(e)}),jQuery(".woocommerce-store-notice__dismiss-link").click(function(){Cookies.set("store_notice","hidden",{path:"/"}),jQuery(".woocommerce-store-notice").hide()}),"hidden"===Cookies.get("store_notice")?jQuery(".woocommerce-store-notice").hide():jQuery(".woocommerce-store-notice").show()});
/*
     FILE ARCHIVED ON 04:44:10 Apr 07, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:05:06 May 11, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 542.603 (3)
  esindex: 0.01
  captures_list: 564.217
  CDXLines.iter: 14.214 (3)
  PetaboxLoader3.datanode: 548.36 (4)
  exclusion.robots: 0.2
  exclusion.robots.policy: 0.175
  RedisCDXSource: 1.937
  load_resource: 12.608
*/