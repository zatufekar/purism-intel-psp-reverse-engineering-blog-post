!function(o){"use strict";var n=function(n){o(this).children(".footnoteContent").first().is(":visible")?o(".inline-footnote .footnoteContent").hide():(o(".inline-footnote .footnoteContent").hide(),o(this).children(".footnoteContent").toggle()),n.stopPropagation()};o(function(){console.log("inlineFootNotesVars",inlineFootNotesVars),inlineFootNotesVars.hover?o(".inline-footnote").hover(n):o(".inline-footnote").click(n),o("html").on("click",function(){o(".inline-footnote .footnoteContent").hide()})})}(jQuery);
/*
     FILE ARCHIVED ON 04:44:20 Apr 07, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:05:00 May 11, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 166.401 (3)
  esindex: 0.012
  captures_list: 185.661
  CDXLines.iter: 12.692 (3)
  PetaboxLoader3.datanode: 184.723 (4)
  exclusion.robots: 0.337
  exclusion.robots.policy: 0.311
  RedisCDXSource: 2.001
  load_resource: 21.898
*/