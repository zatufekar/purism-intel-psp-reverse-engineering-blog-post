!function(a){function b(a){return a=b.buildAjaxOptions(a),b.transport(a)}var c=window.wpApiSettings;b.buildAjaxOptions=function(b){var d,e,f,g,h,i=b.url,j=b.path;if("string"==typeof b.namespace&&"string"==typeof b.endpoint&&(d=b.namespace.replace(/^\/|\/$/g,""),e=b.endpoint.replace(/^\//,""),j=e?d+"/"+e:d),"string"==typeof j&&(i=c.root+j.replace(/^\//,"")),g=!(b.data&&b.data._wpnonce),f=b.headers||{},g)for(h in f)if(f.hasOwnProperty(h)&&"x-wp-nonce"===h.toLowerCase()){g=!1;break}return g&&(f=a.extend({"X-WP-Nonce":c.nonce},f)),b=a.extend({},b,{headers:f,url:i}),delete b.path,delete b.namespace,delete b.endpoint,b},b.transport=a.ajax,window.wp=window.wp||{},window.wp.apiRequest=b}(jQuery);
/*
     FILE ARCHIVED ON 04:44:17 Apr 07, 2018 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 21:05:01 May 11, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 595.309 (3)
  esindex: 0.008
  captures_list: 616.909
  CDXLines.iter: 14.804 (3)
  PetaboxLoader3.datanode: 631.023 (4)
  exclusion.robots: 0.192
  exclusion.robots.policy: 0.169
  RedisCDXSource: 1.654
  load_resource: 51.181
*/